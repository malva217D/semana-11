
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {

        /* Manipulacion Cadenas*/
        String cadena1 = "Curso de Programacion 1 UTN";
        Scanner sc = new Scanner(System.in);

        /* System.out.println(cadena1.length());
        System.out.println(cadena1.toLowerCase());
        
        System.out.println(cadena1.toUpperCase());
        
        System.out.println("\n---Compracion Cadenas de Texto---");
        String c1 = "Programacion";
        String c2 = "Programacion";
        
        System.out.println(c1.compareTo(c2));
        System.out.println(c1.compareToIgnoreCase(c2));
        System.out.println("Uso del substring--\n");
        System.out.println(cadena1.substring(5));
        System.out.println(cadena1.substring(5, 10));
        
        System.out.println(cadena1.contains("UTN")); 
       
        String texto, palabra;
        System.out.println("Ingrese un texto: ");
        texto=sc.nextLine();
        System.out.println("Ingrese la palabra a buscar: ");
        palabra=sc.nextLine();
        boolean buscador = texto.contains(palabra);
        if (buscador) {
            System.out.println("Encontro la Palabra ");
        }else{
            System.out.println("Palabra no existe en el texto ");
        } */
        //System.out.println(cadena1.isEmpty());
        /*String cadena2= "ISW-211 Programacion 1";
        System.out.println(cadena2.startsWith("ISW"));
        System.out.println(cadena2.endsWith("1"));
        
        String ca1 = "curso";
        String ca2 = "Curso";
        
        System.out.println(ca1.equals(ca2));
        System.out.println(ca1.equalsIgnoreCase(ca2));
       
        System.out.println(ca1.charAt(2));*/
 /*String text=" Hola Mundo ";
        System.out.println(text.trim());*/
 /*String dialogo = "El pecio del harina sube si el pecio del gasolina sube";
        
        System.out.println(dialogo.replace("pecio", "precio"));
        System.out.println(dialogo.replace("harina", "maiz"));*/
 /*String textoPlano= "En el mundo hay muchas formas de hacer las cosas diferentes,"
                + "ese mundo es a menudo muy bonito... Hola Mundo";
        String buscar1 = "mundo";
        String frase = buscar1.toLowerCase();
        System.out.println(frase);
        int contador=0;
        
        while (textoPlano.indexOf(frase)>-1){
            textoPlano=textoPlano.substring(textoPlano.indexOf(buscar1)+ buscar1.length(), textoPlano.length()).toLowerCase();
            contador++;
            
        }
        System.out.println("La Palabra "+ buscar1 + " aparece "+ contador+ " veces ");
         */
 /*Funcion Split*/
 /*String materias = "matematicas|ciencias|sociales|español|fultbol";
        String[] datos=materias.split("\\|",5);
        for (String mat : datos) {
            System.out.println(mat);
        }*/
 /*StringTokenizer tokens;
        String nombres = "juan;pedro;marco;gabriel;tatiana";
        File archivo = new File("C:/Prueba/prueba.txt");
        try {
            FileWriter fr = new FileWriter(archivo);
            BufferedWriter br = new BufferedWriter(fr);
            br.write(nombres);
            br.close();
            
        } catch (Exception e) {
            
        }
        
        tokens = new StringTokenizer(nombres, ":");
        while (tokens.hasMoreTokens()) {   
            nombres=nombres+tokens.nextToken();
        }
        System.out.println(nombres);*/
        //frmTexto ventana = new frmTexto();
        //ventana.setVisible(true);
        // Ejercio de Agregar Planilla
        /* ArrayList<Planilla> lista = new ArrayList<Planilla>();
        String archivo = "C:/Deducciones/deducciones.txt";
        FileReader fr = new FileReader(archivo);
        BufferedReader br = new BufferedReader(fr);

        String[] cadena;
        long numeroLinea = 0;

        while ((archivo = br.readLine()) != null) {
            cadena = null;
            cadena = archivo.split("\\|");
            lista.add(new Planilla(Integer.parseInt(cadena[0]), cadena[1].toLowerCase(),
                    Double.parseDouble(cadena[2])));
            numeroLinea++;
        }
        
        System.out.println("La Cantidad de lineas leidas fueron: " + numeroLinea);

        System.out.println("Leyendo los elementos cargados en la planilla \n");

        for (Planilla p : lista) {
            System.out.println(p.toString());
        }*/
        // Ejercicio 2 Patrones en Textos
        /*Pattern p = Pattern.compile(".....s");
        Matcher m = p.matcher("carlos");
        System.out.println(m.matches());*/
        //System.out.println("---Otro Ejemplo---");

        /*Comprobar un correo electronico*/
 /*String input = "www.utn.ac.cr";
        //String patron = "^.|^@";
        //String patron = "^www.";
        String patron = "@";
        Pattern p = Pattern.compile(patron);
        Matcher m = p.matcher(input);

        if (m.find()) {
            System.out.println("La direccion no empieza por punto o @ ");
        }
        if (!m.find()) {
            System.out.println("Los email no empieza con www");

        }
        if (!m.find()) {
            System.out.println("La direccion no es un correo valido");

        }*/
 /* Comparar Redimiento Split vrs Pattern */
        String cadena = "Progra 1; Progra 2; Progra 3; Progra 4";
        //String patron=";";
        String patron = "[B-D]|[F-H]|[J-N]|[P-T]|[V-Z]";

        /* Patter*/
        System.out.println("\n Ejecuntando con el Patter puro \n");
        long inicio1 = System.currentTimeMillis();
        Pattern p = Pattern.compile(patron);
        for (int i = 0; i < 10000; i++) {
            String[] arreglo = p.split(cadena);
        }
        long fin1 = System.currentTimeMillis();
        long resulta1 = fin1 - inicio1;
        System.out.println("La duracion fue de: " + resulta1);

        /* Split*/
        System.out.println("\n Ejecuntando con el Split puro \n");
        Long inicio2 = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            String[] arreglo = cadena.split(patron);
        }
        Long fin2 = System.currentTimeMillis();
        long resulta2 = fin2 - inicio2;
        System.out.println("La duracion fue de: " + resulta2);

    }

}
