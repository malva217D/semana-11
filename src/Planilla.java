public class Planilla {
    
    private int codigo;
    private String nombre;
    private Double monto;

    public Planilla(int codigo, String nombre, Double monto) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.monto = monto;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Planilla{" + "codigo=" + codigo + ", nombre=" + nombre + ", monto=" + monto + '}';
    }
    
    

}
